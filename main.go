package main

import (
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"log/syslog"
	"net"
	"os"
	"strconv"

	"github.com/miekg/dns"
)

const (
	helpText = `The check expects two mandatory arguments:
1. ip address (IPv4-mapped IPv6 address of the DNS server, e.g. ":ffff:a.b.c.d")
2. tcp port number

The rest of the program is controlled by environment variables (defaults in parenthesis):
* SOA_DOMAIN: domain for which we request the SOA record [REQUIRED]
* DEBUG:      when set to anything than 0 enables debugging output to syslog (0)`
)

var (
	hasDebug     = false
	syslogWriter io.Writer
)

func init() {
	var err error
	// Set custom help message
	flag.Usage = func() {
		_, err = fmt.Fprintln(os.Stderr, helpText)
		if err != nil {
			// Do nothing
		}
		os.Exit(127)
	}

	// extract commandline arguments
	flag.Parse()
	if flag.NArg() != 2 {
		log.Fatalf("Got %d commandline arguments, expected exactly two", flag.NArg())
	}

	// enable syslog for debug runs
	if os.Getenv("DEBUG") != "" && os.Getenv("DEBUG") != "0" {
		hasDebug = true
		syslogWriter, err = syslog.New(syslog.LOG_INFO|syslog.LOG_USER, "dns-monitor")
		// if syslog fails, silently discard debug output
		if err != nil {
			//fmt.Fprintf(os.Stderr, "Unable to create syslog writer: %s\n", err.Error())
			syslogWriter = ioutil.Discard
		}
	}

}

// DebugLog is printf for syslog or a NOP, depending on the global hasDebug variable
func DebugLog(format string, args ...interface{}) {
	if hasDebug {
		msg := fmt.Sprintf(format, args...)
		_, err := syslogWriter.Write([]byte(msg))
		if err != nil {
			// Do nothing
		}
	}
}

func main() {
	// F5 provides IPv4 mapped IPv6 address like "::ffff:172.21.0.6" but we want to work with IPv4 form as well
	server := net.ParseIP(os.Args[1])
	if server == nil {
		DebugLog("The given host cannot be parsed as IP address: %s", os.Args[1])
		os.Exit(1)
	}

	port, err := strconv.Atoi(os.Args[2])
	if err != nil {
		DebugLog("The following argument has been provided as port that cannot be converted to integer: %s", os.Args[2])
		os.Exit(1)
	}
	DebugLog("Host: %s, Port: %d", server, port)

	soaDomain := os.Getenv("SOA_DOMAIN")
	DebugLog("SOA Domain to query: %s", dns.Fqdn(soaDomain))
	c := dns.Client{}
	c.Net = "tcp"
    m := dns.Msg{}
    m.SetQuestion(dns.Fqdn(soaDomain), dns.TypeSOA)
    r, t, err := c.Exchange(&m, fmt.Sprintf("%s:%d", server, port))
    if err != nil {
		DebugLog("Error doing the DNS query: %s", err.Error())
		os.Exit(1)
    }
    DebugLog("Query took %v", t)
    if len(r.Answer) == 0 {
		DebugLog("No results for SOA query")
		os.Exit(1)
    }

	// success
	for _, ans := range r.Answer {
        record := ans.(*dns.SOA)
		DebugLog("%s", record)
    }
	fmt.Println("UP")
	DebugLog("SOA query was successful")
}
