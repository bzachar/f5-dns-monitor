# f5-dns-monitor
A custom DNS health monitor for F5 which is using TCP always and queries a SOA record.

# Usage
```
$ /tmp/f5-dns-monitor --help
The check expects two mandatory arguments:
1. ip address (IPv4-mapped IPv6 address of the DNS server, e.g. ":ffff:a.b.c.d")
2. tcp port number

The rest of the program is controlled by environment variables (defaults in parenthesis):
* SOA_DOMAIN: domain for which we request the SOA record [REQUIRED]
* DEBUG:      when set to anything than 0 enables debugging output to syslog (0)

```

# DEBUG
Change the DEBUG variable to 1 and on the F5 box:
```bash
$ journalctl -f
```